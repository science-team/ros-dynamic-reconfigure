Source: ros-dynamic-reconfigure
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Jochen Sprickerhof <jspricke@debian.org>,
 Leopold Palomo-Avellaneda <leo@alaxarxa.net>,
 Timo Röhling <roehling@debian.org>,
Build-Depends:
 catkin (>= 0.8.10-1~),
 debhelper-compat (= 13),
 dh-ros,
 dh-sequence-python3,
 libboost-dev,
 libconsole-bridge-dev,
 libgtest-dev,
 libros-rosgraph-msgs-dev,
 librosconsole-dev,
 libroscpp-core-dev,
 libroscpp-dev,
 libroscpp-msg-dev,
 librostest-dev,
 libstd-msgs-dev,
 libxmlrpcpp-dev,
 python3-all,
 python3-rospy,
 python3-rostest,
 python3-rosunit,
 python3-setuptools,
 ros-message-generation,
Standards-Version: 4.7.0
Section: libs
Rules-Requires-Root: no
Homepage: https://wiki.ros.org/dynamic_reconfigure
Vcs-Browser: https://salsa.debian.org/science-team/ros-dynamic-reconfigure
Vcs-Git: https://salsa.debian.org/science-team/ros-dynamic-reconfigure.git

Package: libdynamic-reconfigure-config-init-mutex0d
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Multi-Arch: same
Description: Robot OS dynamic-reconfigure library
 This package is part of Robot OS (ROS), and contains the
 dynamic_reconfigure package which provides a means to change node
 parameters at any time without having to restart the node.
 .
 This package contains the library itself.

Package: cl-dynamic-reconfigure
Section: lisp
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: Robot OS dynamic-reconfigure library - LISP bindings
 This package is part of Robot OS (ROS), and contains the
 dynamic_reconfigure package which provides a means to change node
 parameters at any time without having to restart the node.
 .
 This package contains the LISP bindings.

Package: libdynamic-reconfigure-config-init-mutex-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libboost-dev,
 libdynamic-reconfigure-config-init-mutex0d ( = ${binary:Version}),
 libroscpp-core-dev,
 libstd-msgs-dev,
 python3,
 ros-message-generation,
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 python3-dynamic-reconfigure,
Description: Robot OS dynamic-reconfigure library - development files
 This package is part of Robot OS (ROS), and contains the
 dynamic_reconfigure package which provides a means to change node
 parameters at any time without having to restart the node.
 .
 This package contains the development files.

Package: python3-dynamic-reconfigure
Section: python
Architecture: all
Depends:
 python3-genpy,
 python3-roslib,
 python3-std-msgs,
 ${misc:Depends},
 ${python3:Depends},
Description: Robot OS dynamic-reconfigure library - Python 3 bindings
 This package is part of Robot OS (ROS), and contains the
 dynamic_reconfigure package which provides a means to change node
 parameters at any time without having to restart the node.
 .
 This package contains the Python 3 bindings.
