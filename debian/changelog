ros-dynamic-reconfigure (1.7.3-2) unstable; urgency=medium

  * Wrap and sort Debian package files
  * Migrate to dh-ros (Closes: #1046940, #1082977)
  * Bump Standards-Version to 4.7.0

 -- Timo Röhling <roehling@debian.org>  Mon, 30 Sep 2024 23:37:48 +0200

ros-dynamic-reconfigure (1.7.3-1) unstable; urgency=medium

  * New upstream version 1.7.3
  * Drop Switch-to-new-boost-bind-bind.hpp.patch (applied upstream)
  * Fix Python interpreter for gendeps

 -- Timo Röhling <roehling@debian.org>  Fri, 06 May 2022 09:49:42 +0200

ros-dynamic-reconfigure (1.7.2-1) unstable; urgency=medium

  * Drop old Breaks/Replaces
  * New upstream version 1.7.2
  * Drop patches (fixed upstream)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 21 Feb 2022 15:23:56 +0100

ros-dynamic-reconfigure (1.7.1-11) unstable; urgency=medium

  * Ignore failing tests on alpha
  * Ignore failing tests on hurd-i386

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 27 Oct 2021 09:06:43 +0200

ros-dynamic-reconfigure (1.7.1-10) unstable; urgency=medium

  * Ignore tests on armhf

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 26 Oct 2021 18:32:51 +0200

ros-dynamic-reconfigure (1.7.1-9) unstable; urgency=medium

  * Fix test exception logic

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 26 Oct 2021 16:10:40 +0200

ros-dynamic-reconfigure (1.7.1-8) unstable; urgency=medium

  * Disable tests on failing archs

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 26 Oct 2021 15:27:28 +0200

ros-dynamic-reconfigure (1.7.1-7) unstable; urgency=medium

  * Enable tests

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 26 Oct 2021 14:23:48 +0200

ros-dynamic-reconfigure (1.7.1-6) unstable; urgency=medium

  * Fix incorrect BEFORE path

 -- Timo Röhling <roehling@debian.org>  Mon, 27 Sep 2021 22:07:44 +0200

ros-dynamic-reconfigure (1.7.1-5) unstable; urgency=medium

  * Team upload.
  * Upload to unstable

 -- Timo Röhling <roehling@debian.org>  Wed, 22 Sep 2021 22:42:08 +0200

ros-dynamic-reconfigure (1.7.1-4) experimental; urgency=medium

  * Team upload.
  * Move pkg-config and CMake config files to /usr/lib/<triplet>
  * Bump Standards-Version to 4.6.0 (no changes)

 -- Timo Röhling <roehling@debian.org>  Mon, 20 Sep 2021 00:28:51 +0200

ros-dynamic-reconfigure (1.7.1-3) unstable; urgency=medium

  * simplify packaging
  * Fix shebang

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 19 Dec 2020 16:15:30 +0100

ros-dynamic-reconfigure (1.7.1-2) unstable; urgency=medium

  * Add patch for Boost 1.74
  * bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 14 Dec 2020 14:22:37 +0100

ros-dynamic-reconfigure (1.7.1-1) unstable; urgency=medium

  * New upstream version 1.7.1
  * rebase patches
  * Remove unused dependencies

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 29 Aug 2020 11:26:55 +0200

ros-dynamic-reconfigure (1.7.0-3) unstable; urgency=medium

  * Ignore test results

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 27 Jun 2020 10:08:28 +0200

ros-dynamic-reconfigure (1.7.0-2) unstable; urgency=medium

  * Use local IP for tests

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 19 Jun 2020 23:04:57 +0200

ros-dynamic-reconfigure (1.7.0-1) unstable; urgency=medium

  * New upstream version 1.7.0
  * Remove Thomas from Uploaders, thanks for working on this
  * Add patch to make builds reproducible
  * rebase patches
  * bump debhelper version

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 19 Jun 2020 09:11:58 +0200

ros-dynamic-reconfigure (1.6.0-4) unstable; urgency=medium

  * Drop unused build dependency (Closes: #962286)
  * bump policy

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 05 Jun 2020 18:25:58 +0200

ros-dynamic-reconfigure (1.6.0-3) unstable; urgency=medium

  * Update Recommends

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 26 Oct 2019 09:44:56 +0200

ros-dynamic-reconfigure (1.6.0-2) unstable; urgency=medium

  * Drop Python 2 packages (Closes: #938374)
  * simplify d/watch
  * switch to debhelper-compat and debhelper 12
  * Bump policy version (no changes)
  * add Salsa CI

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 26 Oct 2019 07:04:05 +0200

ros-dynamic-reconfigure (1.6.0-1) unstable; urgency=medium

  * New upstream version 1.6.0
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 20 Oct 2018 20:07:31 +0200

ros-dynamic-reconfigure (1.5.49-4) unstable; urgency=medium

  * Bump policy version (no changes)
  * Add Python 3 package

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 19 Aug 2018 18:02:57 +0200

ros-dynamic-reconfigure (1.5.49-3) unstable; urgency=medium

  * Fix roscpp-msg* dependency
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 31 Jul 2018 21:58:19 +0200

ros-dynamic-reconfigure (1.5.49-2) unstable; urgency=medium

  * Update Vcs URLs to salsa.d.o
  * Move cleanup to d/clean
  * Add R³
  * http -> https
  * Bump policy and debhelper versions

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 22 Jun 2018 17:39:25 +0200

ros-dynamic-reconfigure (1.5.49-1) unstable; urgency=medium

  * New upstream version 1.5.49
  * add Multi-Arch according to hinter
  * Rebase patch
  * Update policy and debhelper versions
  * Update copyright
  * Update watch file
  * Remove parallel (Default for debhelper 10)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 24 Sep 2017 07:57:18 +0200

ros-dynamic-reconfigure (1.5.46-1) unstable; urgency=medium

  * New upstream version 1.5.46

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 16 Nov 2016 08:35:16 +0100

ros-dynamic-reconfigure (1.5.45-1) unstable; urgency=medium

  * New upstream version 1.5.45

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 04 Nov 2016 09:41:43 +0100

ros-dynamic-reconfigure (1.5.44-3) unstable; urgency=medium

  * Update my email address
  * Add recommends because catkin needs it

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 21 Aug 2016 23:04:17 +0200

ros-dynamic-reconfigure (1.5.44-2) unstable; urgency=medium

  * rebuild for changes in ros-genlisp

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Wed, 13 Jul 2016 22:19:14 +0200

ros-dynamic-reconfigure (1.5.44-1) unstable; urgency=medium

  * Imported Upstream version 1.5.44
  * Bumped Standards-Version to 3.9.8. No changes

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Thu, 30 Jun 2016 09:28:10 +0200

ros-dynamic-reconfigure (1.5.39-3) unstable; urgency=medium

  * Adopt to new libexec location in catkin
  * Updated Vcs-Browser and Vcs-Git fields
  * Enabling hardening=+all,+bindnow options to mute lintian

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Tue, 23 Feb 2016 15:01:58 +0100

ros-dynamic-reconfigure (1.5.39-2) unstable; urgency=medium

  * Add dependencies

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sun, 10 Jan 2016 15:03:22 +0100

ros-dynamic-reconfigure (1.5.39-1) unstable; urgency=medium

  * Initial package. (Closes: #804929)

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Thu, 24 Dec 2015 00:14:21 +0000
